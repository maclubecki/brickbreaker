#ifndef BALL_H_INCLUDED
#define BALL_H_INCLUDED

class Ball
{
    private:

        int x, y;
        int vel;
        float dirX, dirY;

        int state; // 0 = out, 1 = held, 2 = moving
        SDL_Rect clip;

    public:

        Ball();

        int getX();
        int getY();
        int& getV();

        void show( int cx, int cy );
        void update_clip( std::vector<Powerup> &powers );
        void move( float dt, int padx, bool padmot , std::vector <Brick> &vect , std::vector <Brick> &killvect, std::vector <Powerup> &powers, std::vector <Particle> &effects );
        void change_state( int st );
        void deathcheck( int &lives, gamestate_t &gState, int &padVX );
};


#endif // BALL_H_INCLUDED
