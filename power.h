#ifndef POWER_H_INCLUDED
#define POWER_H_INCLUDED

class Powerup
{
    private:

        //movement related
        int x, y;
        int originX, originY;
        int vel;
        float dirX, dirY;
        //time related
        long expireTime;
        int duration;
        //misc
        bool active;
        int ammo;
        powertype_t type;
        SDL_Rect clip;

    public:

        Powerup( int _x, int _y, int _dirX, powertype_t _type );
        void move( float dt );
        void update( float dt );
        void show();
        void fired();
        void pickUp( bool duplicate, int &speed );
        void extendTime( int time );
        bool shouldDie();

        //gets
        int getX();
        int getY();
        bool getActivity();
        powertype_t getType();
        SDL_Rect* getClip();
};


#endif // POWER_H_INCLUDED
