#include "globals.h"

//float minimum
float min( float a, float b )
{
    if( a <= b ) return a;
    else return b;
}
//point intersection
bool intersects_pad( int xObj, int yObj, int xPad )
{
    if( ( xObj >= xPad ) && ( xObj <= xPad + PAD_W ) )
    {
        if( ( yObj <= PAD_Y + PAD_H ) && ( yObj >= PAD_Y ) )
        {
            return true;
        }
    }

    return false;
}
//RNG
int random_gen( int a, int b, int x = NULL )
{
    return ( a + ( rand() % ( b-a+1 ) ) );
}
//loading imgs
SDL_Surface* load_img( std::string filename )
{
    SDL_Surface* loaded = NULL;
    SDL_Surface* optimized = NULL;

    loaded = IMG_Load( filename.c_str() );

    if( loaded != NULL )
    {
        optimized = SDL_DisplayFormat( loaded );
        SDL_FreeSurface( loaded );
        SDL_SetColorKey( optimized, SDL_SRCCOLORKEY, SDL_MapRGB( optimized->format, 0, 0xFF, 0xFF ) );
    }

    return optimized;
}
//applying imgs
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* dest , SDL_Rect* clip = NULL )
{
    SDL_Rect offset;

    offset.x = x;
    offset.y = y;

    SDL_BlitSurface( source, clip, dest, &offset);
}
//initializing all
bool init()
{

    //video
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1)
        return false;

    screen = SDL_SetVideoMode( WID, HIG, BPP, SDL_HWSURFACE );
    if( screen == NULL )
        return false;

    //fonts
    if( TTF_Init() == -1 )
    {
        return false;
    }

    //audio
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) == -1 )
    {
        return false;
    }


    SDL_WM_SetCaption( "BrickBreaker", NULL );

    return true;
}
//mass load files
bool load_files()
{

    //loading images:
    //background
    img = load_img( "./gfx/bground.png" );
    if( img == NULL )
        return false;
    //logo
    menu_logo = load_img( "./gfx/logo.png" );
    if( menu_logo == NULL )

        return false;
    //menu buttons
    menu_buttons = load_img( "./gfx/menu_buttons.png");
    if( menu_buttons == NULL )
        return false;
    //pad
    pad_surf = load_img( "./gfx/pad.png" );
    if( pad_surf == NULL )
        return false;
    //ball
    ball_surf = load_img( "./gfx/ball.png" );
    if( ball_surf == NULL )
        return false;
    //particle sprite sheet
    particles = load_img( "./gfx/particles.png" );
    if( particles == NULL )
        return false;
    //level complete msg
    gratz_surf = load_img( "./gfx/lvl_complete.png" );
    if( gratz_surf == NULL )
        return false;
    //error msg sprite
    error_msg = load_img( "./gfx/error.png" );
    if( error_msg == NULL )
        return false;

    //POWERUP SPIRTES
    pow_package = load_img( "./gfx/pack.png" );
    if( pow_package == NULL )
        return false;
    pow_sheet = load_img( "./gfx/power_sheet.png");
    if( pow_sheet == NULL )
        return false;

    //BRICK SPRITES
    brick_sheet = load_img( "./gfx/brick_sheet.png" );
    if( brick_sheet == NULL )
        return false;


    //loading font
    font_score = TTF_OpenFont( "visitor1.ttf", 18 );
    if( font_score == NULL )
        return false;

    //loading sound
    SFX_bump_pad = Mix_LoadWAV( "./sfx/pad.wav" );
    SFX_bump_wall = Mix_LoadWAV( "./sfx/wall.wav" );
    SFX_hit = Mix_LoadWAV( "./sfx/hit.wav" );
    SFX_firelaser = Mix_LoadWAV( "./sfx/laser.wav" );
    SFX_laserhit = Mix_LoadWAV( "./sfx/laserhit.wav" );
    SFX_pickup = Mix_LoadWAV( "./sfx/pickup.wav" );
    SFX_death = Mix_LoadWAV( "./sfx/death.wav" );
    SFX_explode = Mix_LoadWAV( "./sfx/explode.wav" );
    SFX_exploding = Mix_LoadWAV( "./sfx/exploding.wav" );
    SFX_drop = Mix_LoadWAV( "./sfx/drop.wav" );
    SFX_balldie = Mix_LoadWAV( "./sfx/balldie.wav" );
    SFX_lvlwon = Mix_LoadWAV( "./sfx/lvlwon.wav" );

    return true;
}
//cleanup
void cleanup()
{

    //free surfaces
    SDL_FreeSurface( img );
    SDL_FreeSurface( error_msg );
    SDL_FreeSurface( loge );
    SDL_FreeSurface( surf_score );
    SDL_FreeSurface( surf_lives );
    SDL_FreeSurface( surf_powers_txt );
    SDL_FreeSurface( gratz_surf );
    SDL_FreeSurface( ball_surf );
    SDL_FreeSurface( pad_surf );
    SDL_FreeSurface( particles );
    SDL_FreeSurface( menu_buttons );
    SDL_FreeSurface( brick_sheet );
    SDL_FreeSurface( pow_package );
    SDL_FreeSurface( menu_buttons_start );
    SDL_FreeSurface( menu_buttons_end );
    SDL_FreeSurface( surf_level );

    //close font and quit ttf
    TTF_CloseFont( font_score );
    TTF_Quit();

    //free all sound chunks and quit mixer
    Mix_FreeChunk( SFX_bump_pad );
    Mix_FreeChunk( SFX_bump_wall );
    Mix_FreeChunk( SFX_hit );
    Mix_FreeChunk( SFX_firelaser );
    Mix_FreeChunk( SFX_pickup );
    Mix_FreeChunk( SFX_drop );
    Mix_FreeChunk( SFX_death );
    Mix_FreeChunk( SFX_explode );
    Mix_FreeChunk( SFX_exploding );
    Mix_FreeChunk( SFX_balldie );
    Mix_FreeChunk( SFX_lvlwon );
    Mix_FreeChunk( SFX_laserhit );
    Mix_CloseAudio();

    SDL_Quit();
}
