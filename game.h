#include "globals.h"
#include "bricks.h"
#include "particle.h"
#include "power.h"
#include "global_brick_fs.h"
#include "bullet.h"
#include "ball.h"
#include "paddle.h"
#include "button.h"
#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

class Game
{
    private:

        int mouse_x, mouse_y;
        int lives;
        int level;
        int msgVel;
        long pause_time;
        long score;
        Pad playerPaddle;
        Ball playBall;

        gamestate_t state;
        Button *startBtn;
        Button *quitBtn;
        Camera cam;
        Point msgOrigin;

        //containers
        std::vector <Brick> bricks;
        std::vector <Brick> bricks_broken;
        std::vector <Particle> effects;
        std::vector <Powerup> powers_active;
        std::vector <Powerup> powers_inactive;
        std::vector <Bullet> bullets;

        //level initializing functions
        int init_bricks_setGaps( int r_cols );
        int init_bricks_setShift( int r_cols, int r_gaps );
        type_t init_bricks_setTypes( int r_cols, int i );
        void init_bricks();
        //cleanup functions
        void clearParticles();
        void clearBullets();
        void clearPowerups( std::vector <Powerup> &powers );


    public:

        Game();
        gamestate_t getState();
        void setState( gamestate_t s );

        bool winCheck();
        void init_powers();
        void startup( bool &quit );
        void play( bool &quit, float dt, float &accumulator );
        void msgScreen( float dt, bool wonGame );
        void paused( bool &quit, float &frame_time );
        void handleMouseInput( SDL_Event &e, bool &quit );
        void showUI();
        void displayEverything();
        void updatePowerUps();
};

#endif // GAME_H_INCLUDED
