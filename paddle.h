#ifndef PADDLE_H_INCLUDED
#define PADDLE_H_INCLUDED

class Pad
{
    private:

        int x, prevx;
        bool isMoving;

    public:

        int xVel;

        Pad();

        int getX();

        bool getMotion();

        void setMotion( bool a );
        void handle_input( Ball &b, gamestate_t &state, long &paused_ticks, std::vector <Powerup> &powers_act, std::vector <Bullet> &bullets );
        void move( float dt, std::vector <Particle> &effects, std::vector <Powerup> &powers_act, std::vector <Powerup> &powers_inact, int &life, int &speed );
        void power_collision( std::vector <Particle> &effects, std::vector <Powerup> &powers_act, std::vector <Powerup> &powers_inact, int &life, int &speed );
        void show( int cx, int cy );
        void center();

};


#endif // PADDLE_H_INCLUDED
