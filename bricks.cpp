#include "globals.h"
#include "bricks.h"

Brick::Brick( int posx, int posy, int p_row, int p_column, type_t typ )
{
    x = posx;
    y = posy;
    clip.x = 0;
    clip.w = BRICK_W;
    clip.h = BRICK_H;

    row = p_row;
    column = p_column;

    brickType = typ;

    isbroken = false;
    time_odeath = 0;

    switch( typ )
    {
        case REGULAR:
            hp = 1;
            points = 50;
            clip.y = 0;
            break;
        case REINFORCED:
            hp = 3;
            points = 200;
            clip.y = 100;
            break;
        case EXPLOSIVE:
            hp = 1;
            points = 100;
            clip.y = 20;
            break;
        case INDESTRUCTIBLE:
            hp = 300;
            points = 300;
            clip.y = 40;
            break;
    }
}

void Brick::show( int cx, int cy )
{
    apply_surface( x + cx, y + cy, brick_sheet, screen, &clip );
}
void Brick::hit( bool blown )
{
    if( blown == false )
    {
        hp--;
        if( brickType == REINFORCED )
        {
            if( hp == 2 ) clip.y = 80;
            if( hp == 1 ) clip.y = 60;
        }
    }
    else
    {
        hp = 0;
    }
}

type_t Brick::getType()
{
    return brickType;
}
powertype_t Brick::getDropType()
{
    return dropType;
}

int Brick::getX()
{
    return x;
}
int Brick::getY()
{
    return y;
}
int Brick::getRow()
{
    return row;
}
int Brick::getColumn()
{
    return column;
}
int Brick::getHP()
{
    return hp;
}
int Brick::getPoints()
{
    return points;
}
long Brick::getTOD()
{
    return time_odeath;
}
bool Brick::getIsbroken()
{
    return isbroken;
}
bool Brick::getDoesDrop()
{
    return doesDrop;
}

void Brick::setIsbroken()
{
    isbroken = true;
}
void Brick::setTOD( long x )
{
    time_odeath = x;
}
void Brick::setDrop( int i )
{
    int r;

    //determine drop type
    r = random_gen( 0, 7, i );
    switch( r )
    {
        case 0:
            dropType = POW_PHASEBALL;
            break;
        case 1:
            dropType = POW_PHAZOR;
            break;
        case 2:
            dropType = POW_DOUBLEPTS;
            break;
        case 3:
            dropType = POW_FASTBALL;
            break;
        case 4:
            dropType = POW_SLOWBALL;
            break;
        case 5:
            dropType = POW_FIREBALL;
            break;
        case 6:
            dropType = POW_LAZOR;
            break;
        case 7:
            dropType = POW_LIFE;
            break;

        default:
            dropType = POW_LAZOR;
            break;
    }

    //does it drop?
    if( brickType != EXPLOSIVE )
    {
        r = random_gen( 0, 100, i);
        if( r >= 95 ) doesDrop = true;
        else doesDrop = false;
    }
    else doesDrop = false;

}
void Brick::setType( type_t newType )
{
    brickType = newType;
}