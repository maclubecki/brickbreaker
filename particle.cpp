#include "particle.h"

Particle::Particle( int _x, int _y, particle_t _type, int _magnitude )
{
    x = _x;
    y = _y;
    spawn_time = SDL_GetTicks();
    frame_current = 1; //frame numbers start from 1
    p_alpha = 255;
    type = _type;
    dead = false;

    if( _type == SCORE )
    {
        vel = 200;
        dirX = 0;
        dirY = -1;

        duration = 100;
        frames = 1;

        if( _magnitude == 50 )
        {
            clip.x = 0;
            clip.y = 12;
            clip.w = 7;
            clip.h = 5;
        }
        if( _magnitude == 100 )
        {
            clip.x = 8;
            clip.y = 12;
            clip.w = 12;
            clip.h = 7;
        }
        if( _magnitude == 200 )
        {
            clip.x = 21;
            clip.y = 12;
            clip.w = 14;
            clip.h = 7;
        }
        if( _magnitude == 300 )
        {
            clip.x = 36;
            clip.y = 12;
            clip.w = 14;
            clip.h = 7;
        }
        if( _magnitude == 400 )
        {
            clip.x = 0;
            clip.y = 21;
            clip.w = 20;
            clip.h = 10;
        }
        if( _magnitude == 600 )
        {
            clip.x = 21;
            clip.y = 21;
            clip.w = 20;
            clip.h = 10;
        }
    }
    if( _type == EXPLOSION )
    {
        vel = 0;
        dirX = 0;
        dirY = 0;

        duration = 300;
        frames = 4;

        if( _magnitude == 1 )
        {
            clip.x = 0;
            clip.y = 0;
            clip.w = 13;
            clip.h = 11;
        }
    }
    if( _type == SPARKS || _type == PICKUP)
    {
        //they're called sparks but they're the flying chunks that leave a trail
        vel = 500;
        duration = 350;
        frames = 1;

        //the direction is set through this function
        set_direction( _magnitude );

        clip.x = 55;
        if( _type == PICKUP ) clip.y = 6;
        else clip.y = 2;
        clip.w = 4;
        clip.h = 4;
    }
    if( _type == TRAIL )
    {
        vel = 0;
        dirX = 0;
        dirY = 0;
        p_alpha = 200;

        duration = 170;
        frames = 1;

        if( _magnitude == 0 )
            clip.x = 55;
        if( _magnitude == 12 )
            clip.x = 57;
        if( _magnitude == 24 )
            clip.x = 59;
        clip.y = 0;
        clip.w = 2;
        clip.h = 2;
    }
    if( _type == TRAIL_PAD )
    {
        vel = 0;
        dirX = 0;
        dirY = 0;
        p_alpha = 150;

        duration = 100;
        frames = 1;

        clip.x = 57;
        clip.y = 0;
        clip.w = 4;
        clip.h = 2;
    }
    if( _type == TRAIL_SPARK )
    {
        vel = 0;
        dirX = 0;
        dirY = 0;
        p_alpha = 200;

        duration = 150;
        frames = 1;

        if( _magnitude == 0 ) clip.x = 54;
        else clip.x = 59;
        clip.y = 0;
        clip.w = 1;
        clip.h = 1;
    }
    if( _type == TRAIL_BULLET )
    {
        vel = 0;
        dirX = 0;
        dirY = 0;
        p_alpha = 200;

        duration = 100;
        frames = 1;

        if( _magnitude == 1 )
        {
            clip.x = 64;
        }
        else
        {
            clip.x = 61;
        }
        clip.y = 0;
        clip.w = 3;
        clip.h = 10;
    }
}
void Particle::update( float dt, std::vector <Particle> &particles )
{
    if( ( SDL_GetTicks() >= ( spawn_time + duration ) ) || ( p_alpha <= 0 ) )
    {
        dead = true;
        return;
    }
    if( SDL_GetTicks() - spawn_time >= ( frame_current * duration / frames ) )
    {
        frame_current++;
        getClip();
    }
    if( ( type == TRAIL || type == TRAIL_SPARK || type == SPARKS ) && p_alpha > 0 )
    {
        p_alpha -= 10;

        if( type == TRAIL_SPARK )
            p_alpha -= 10;
    }
    if( type == SPARKS || type == PICKUP )
    {
        dirY += 0.03;
        vel -= 18;
    }

    move( dt, particles );
}
void Particle::move( float dt, std::vector <Particle> &particles )
{
    int dx, dy;
    float vectorLength;

    dx = (int)(dirX * vel * dt);
    dy = (int)(dirY * vel * dt);

    if( type == SPARKS || type == PICKUP )
    {
        int trail_id = 0;
        if( type == PICKUP ) trail_id = 1;

        //making a trail
        Particle *a;
        particles.push_back( *( a = new Particle( x, y, TRAIL_SPARK, trail_id ) ) );
        delete a;
    }

    x += dx;
    y += dy;

}
void Particle::set_direction( int mag )
{
    switch( mag )
    {
        case 1: //up
                dirX = 0;
                dirY = -1;
                break;
        case 2: //right-up
                dirX = 1;
                dirY = -1;
                break;
        case 3: //right
                dirX = 1;
                dirY = 0;
                break;
        case 4: //right-down
                dirX = 1;
                dirY = 1;
                break;
        case 5: //down
                dirX = 0;
                dirY = 1;
                break;
        case 6: //left-down
                dirX = -1;
                dirY = 1;
                break;
        case 7: //left
                dirX = -1;
                dirY = 0;
                break;
        case 8: //left-up
                dirX = -1;
                dirY = -1;
                break;

    }
    if( mag % 2 == 0 )
    {
        //the cross has a longer duration
        duration += 200;
    }
    if( mag != 7 && mag != 3 )
    {
        duration = 85;
    }

}
void Particle::show( int cx, int cy )
{
    SDL_SetAlpha( particles, SDL_SRCALPHA, p_alpha );
    apply_surface( x + cx, y + cy, particles, screen, &clip );
}
void Particle::getClip()
{
    if( type == EXPLOSION )
    {
        if( frame_current == 2 )
        {
            clip.x = 13;
        }
        if( frame_current == 3 )
        {
            clip.x = 25;
        }
        if( frame_current == 4 )
        {
            clip.x = 39;
        }
    }
}
bool Particle::isDead()
{
    return dead;
}