#include "game.h"
#include <fstream>
//MAIN
int main( int argc, char* args[] )
{
    long tiem_old, tiem_new;
    float tiem_frame;
    float accumulator = 0.0;
    const float dt = 0.01;

    bool quit = false;
    Game g;

    if( init() == false )
        return 1;
    if( load_files() == false)
        return 1;

    tiem_old = SDL_GetTicks();
    srand( SDL_GetTicks() );

    try
    {
        while( quit == false)
        {
            tiem_new = SDL_GetTicks();
            tiem_frame = ( tiem_new - tiem_old )/1000.f;
            tiem_old = tiem_new;

            accumulator += tiem_frame;

            if( g.getState() == STATE_MENU )
            {
                g.startup( quit );
            }
            if( g.getState() == STATE_PLAYING )
            {
                g.play( quit, dt, accumulator );
            }
            if( g.getState() == STATE_PAUSED )
            {
                g.paused( quit, accumulator );
            }
            if( g.getState() == STATE_WON )
            {
                g.msgScreen( dt, true );
            }
            if( g.getState() == STATE_LOST )
            {
                g.msgScreen( dt, false );
            }

            //Updating screen
            if( SDL_Flip( screen ) == -1 )
                return 1;
        }
    }
    catch(const char *exc)
    {
          std::ofstream myfile;
          myfile.open ("error.txt");
          myfile << "Error: " << exc;
          myfile.close();
    }


    cleanup();
    return 0;
}
