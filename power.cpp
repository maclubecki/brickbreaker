#include "power.h"

Powerup::Powerup( int _x, int _y, int _dirX, powertype_t _type )
{
    //common properties
    originX = _x;
    x = originX;
    originY = _y;
    y = originY;

    dirX = _dirX;
    dirY = -1.0;
    vel = 400;

    type = _type;
    clip.x = _type * 14;
    clip.y = 0;
    clip.w = 14;
    clip.h = 14;
    active = false;

    //for those ups that might not have one of these
    ammo = 1;
    duration = 0;

    //individual properties
    if( _type == POW_LAZOR )
    {
        ammo = 5;
    }
    if( _type == POW_PHAZOR)
    {
        ammo = 3;
    }
    if( _type == POW_FIREBALL)
    {
        duration = 35000;
    }
    if( _type == POW_PHASEBALL)
    {
        duration = 20000;
    }
    if( _type == POW_DOUBLEPTS)
    {
        duration = 15000;
    }
}
//gets
bool Powerup::getActivity()
{
    return active;
}
int Powerup::getX()
{
    return x;
}
int Powerup::getY()
{
    return y;
}
powertype_t Powerup::getType()
{
    return type;
}
SDL_Rect* Powerup::getClip()
{
    return &clip;
}
void Powerup::fired()
{
    ammo--;
}

void Powerup::show()
{
    //rendering the package sprite and then the powerup sprite ON it
    if( !active )
    {
        apply_surface( x, y, pow_package, screen );
        apply_surface( x + 8 , y + 8, pow_sheet, screen, &clip );
    }
}
void Powerup::move( float dt )
{
    int dx, dy;

    dx = (int)(dirX * vel * dt);
    dy = (int)(dirY * vel * dt);

    x += dx;
    y += dy;
}
void Powerup::update( float dt )
{
    //bouncing from the walls
    if( ( x < 100 ) || ( x > 654 ) )
    {
        //keeping them borders
        if( x < 100 ) x = 101;
        if( x > 654 ) x = 653;

        dirX = -dirX;
    }
    //bouncing from the ceiling
    if ( y < 50 )
    {
        y = 51;
        dirY = -dirY;
    }

    dirY += 0.05;
    if( dirY > 0 ) vel = 150;

    move( dt );
}
void Powerup::pickUp( bool duplicate, int &speed )
{
    if( !duplicate )
    {
        active = true;
        expireTime = SDL_GetTicks() + duration;
        if( type == POW_SLOWBALL )
        {
            if( speed - 50 >= 200 ) speed -= 50;
            else speed = 200;
        }
        if( type == POW_FASTBALL )
        {
            if( speed + 50 <= BALL_MAXV ) speed += 50;
            else speed = BALL_MAXV;
        }
    }
    else
    {
        //if its a duplicate, update it's properties:
        //extend time
        expireTime += duration;
        //or renew ammo if it's a laser/phaser
        if( type == POW_LAZOR )
        {
            ammo = 5;
        }
        if( type == POW_PHAZOR)
        {
            ammo = 3;
        }
    }
}
void Powerup::extendTime( int time )
{
    expireTime += time;
}
bool Powerup::shouldDie()
{
    //check for:
    //  out of bounds
    //  duration expired
    //  no ammo left

    if( active )
    {
        if( ( type == POW_LAZOR || type == POW_PHAZOR ) && ammo <= 0 ) return true;
        if( !( type == POW_LAZOR || type == POW_PHAZOR ) && SDL_GetTicks() > expireTime ) return true;
    }
    else
    {
        if( y > PAD_Y + 25) return true;
    }

    return false;
}