The first major C++/SDL project I've been working on since summer of 2013 till early 2014. Overall it's a standard Arkanoid clone, but the main goal was to generate the levels randomly, which works quite well. 

![1.jpg](https://bitbucket.org/repo/r5Gqek/images/3659893389-1.jpg)
![2.jpg](https://bitbucket.org/repo/r5Gqek/images/4045246493-2.jpg)
![3.jpg](https://bitbucket.org/repo/r5Gqek/images/2856905740-3.jpg)