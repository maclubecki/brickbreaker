#ifndef PARTICLE_H_INCLUDED
#define PARTICLE_H_INCLUDED

class Particle
{
    private:

        int x, y;
        int vel;
        int acceleration;
        float dirX, dirY;

        SDL_Rect clip;
        particle_t type;
        int p_alpha;
        int frames;
        int frame_current;
        int duration;
        long spawn_time;

        bool dead;

    public:

        Particle( int _x, int _y, particle_t _type, int _magnitude );

        int getX();
        int getY();
        int getVel();
        bool isDead();

        void set_direction( int mag );
        void update( float dt, std::vector <Particle> &particles);
        void getClip();
        void move( float dt, std::vector <Particle> &particles );
        void show( int cx, int cy );

};


#endif // PARTICLE_H_INCLUDED
