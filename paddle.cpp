#include "paddle.h"

Pad::Pad()
{
    x = prevx = 350;
    xVel = 0;

    isMoving = false;
}

int Pad::getX()
{
    return x;
}
bool Pad::getMotion()
{
    return isMoving;
}

void Pad::setMotion( bool a )
{
    isMoving = a;
}
void Pad::power_collision( std::vector <Particle> &effects, std::vector <Powerup> &powers_act, std::vector <Powerup> &powers_inact, int &life, int &speed )
{
    int index = 0;
    int eraseIndex = 0;
    bool found_duplicate = false;
    bool collided = false;

    for( int i = 0; i < powers_inact.size() ; i++ )
    {
        //check collsions
        collided = intersects_pad( powers_inact.at(i).getX() + 14, powers_inact.at(i).getY() + 29, x );

        if( collided )
        {
            //playing sound
            Mix_PlayChannel( -1, SFX_pickup, 0 );
            //pickup particles
            spawn_pickup( powers_inact.at(i).getX() + 14, powers_inact.at(i).getY() + 29, effects );

            //life powerup is instant, so it's done here:
            if( powers_inact.at(i).getType() == POW_LIFE )
            {
                if( life <= 9 ) life++;
                eraseIndex = i;
                break;
            }

            //look for a duplicate power that is already active
            found_duplicate = search4power( index, powers_inact.at(i).getType(), powers_act );

            if( found_duplicate )
            {
                //1. call the pickUp function for the found duplicate
                //!NOTICE that it's [index], not [i]
                powers_act.at(index).pickUp( true, speed );
                //2. set the eraseIndex variable so that it stores the index of the
                //inactive power we just 'activated'
                eraseIndex = i;
            }
            else
            {
                //1. add the inactive power to the list of active powers
                powers_act.push_back( powers_inact.at(i) );
                //2. call pickUp for the newly activated power
                powers_act.at( powers_act.size() - 1 ).pickUp( false, speed );
                //3. store the index of that same power in the eraseIndex variable
                eraseIndex = i;
            }

            //exit the loop if done
            break;
        }
    }

    //if there was a collision (that is, a powerup has been picked up),
    //erasing the inactive power, wheter it was a duplicate or not
    if( collided )
    {
        powers_inact.erase( powers_inact.begin() + eraseIndex );
    }
}
void Pad::move( float dt, std::vector <Particle> &effects, std::vector <Powerup> &powers_act, std::vector <Powerup> &powers_inact, int &life, int &speed )
{
    int dx;
    dx = (int)(xVel * dt);

    if( dx < 0 )
    {
        spawn_particle( x + PAD_W - 5, PAD_Y + 15 , TRAIL_PAD, 0, effects );
    }
    else if( dx > 0 )
    {
        spawn_particle( x, PAD_Y + 15, TRAIL_PAD, 0, effects );
    }

    x += dx;

    if( x < 100 || x + PAD_W > WID - 100 )
        x -= dx;

    power_collision( effects, powers_act, powers_inact, life, speed );
}
void Pad::show( int cx, int cy )
{
    apply_surface( x + cx, PAD_Y + cy, pad_surf, screen );
}
void Pad::handle_input( Ball &b, gamestate_t &state, long &paused_ticks, std::vector <Powerup> &powers_act, std::vector <Bullet> &bullets )
{
    int u;
    bool active_laser = false;
    bool active_phaser = false;

    if( search4power( u, POW_LAZOR, powers_act ) ) active_laser = true;
    if( search4power( u, POW_PHAZOR, powers_act ) ) active_phaser = true;

    //moving mouse
    if( event.type == SDL_MOUSEMOTION )
    {
        int a; a = event.motion.x;

        //keeping the pad within borders
        if( a - PAD_W/2 > 100 && a + PAD_W/2 < WID - 100 )
        {
            x = a - PAD_W/2;
        }
        else
        {
            //could use warps to keep the mouse within borders
            if( a < 100 )
            {
                x = 101;
            }
            if( a > WID - 100 )
            {
                x = WID - 100 - PAD_W;
            }
        }
    }
    //MOUSE INPUT
    //pressing mouse button
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            b.change_state(2);
        }
    }
    //releasing mouse button
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            if( active_phaser )
            {
                //playing sound
                Mix_PlayChannel( -1, SFX_firelaser, 0 );

                Bullet *b;
                bullets.push_back( *( b = new Bullet( x, true ) ) );
                delete b;

                powers_act.at(u).fired();
            }
            else if( active_laser )
            {
                //playing sound
                Mix_PlayChannel( -1, SFX_firelaser, 0 );

                Bullet *b;
                bullets.push_back( *( b = new Bullet( x, false ) ) );
                delete b;

                powers_act.at(u).fired();
            }

        }
    }

    //KEYBOARD INPUT
    //key pressed
    if( event.type == SDL_KEYDOWN )
    {
        switch( event.key.keysym.sym )
        {
            case SDLK_LEFT:

                setMotion( true );
                xVel -= PAD_V;
                break;

            case SDLK_RIGHT:

                setMotion( true );
                xVel += PAD_V;
                break;

            case SDLK_SPACE:

                b.change_state(2);
                if( active_phaser )
                {
                    //playing sound
                    Mix_PlayChannel( -1, SFX_firelaser, 0 );

                    Bullet *b;
                    bullets.push_back( *( b = new Bullet( x, true ) ) );
                    delete b;

                    powers_act.at(u).fired();
                }
                else if( active_laser )
                {
                    //playing sound
                    Mix_PlayChannel( -1, SFX_firelaser, 0 );

                    Bullet *b;
                    bullets.push_back( *( b = new Bullet( x, false ) ) );
                    delete b;

                    powers_act.at(u).fired();
                }
                break;
        }
    }
    //key released
    if( event.type == SDL_KEYUP )
    {
        switch( event.key.keysym.sym )
        {
            case SDLK_LEFT:

                setMotion( false );
                xVel += PAD_V;
                break;

            case SDLK_RIGHT:

                setMotion( false );
                xVel -= PAD_V;
                break;

            case SDLK_ESCAPE://pausing the game

                state = STATE_PAUSED;
                paused_ticks = SDL_GetTicks();
                Mix_PlayChannel( -1, SFX_pickup, 0 );
                SDL_ShowCursor(1);
                break;
        }
    }
}
void Pad::center()
{
    x = WID/2 - PAD_W/2;
    xVel = 0;
}