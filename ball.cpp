#include "ball.h"

Ball::Ball()
{
    x = 380;
    y = 450;

    dirX = 0.5;
    dirY = 1.0;
    vel = 0;

    state = 1;

    clip.x = 0;
    clip.y = 0;
    clip.w = 12;
    clip.h = 12;
}

int Ball::getX()
{
    return x;
}
int Ball::getY()
{
    return y;
}
int& Ball::getV()
{
    return vel;
}

void Ball::show( int cx, int cy )
{
    if( state != 0)
    {
        apply_surface( x + cx, y + cy, ball_surf, screen, &clip );
    }
}
void Ball::update_clip( std::vector<Powerup> &powers )
{
    int useless;

    if( search4power( useless, POW_PHASEBALL, powers ) )
    {
        clip.x = 24;
        return;
    }
    if( search4power( useless, POW_FIREBALL, powers ) )
    {
        clip.x = 12;
        return;
    }
    //if just a regular ball
    clip.x = 0;
}
void Ball::move( float dt, int padx, bool padmot, std::vector <Brick> &vect, std::vector <Brick> &killvect, std::vector <Powerup> &powers, std::vector <Particle> &effects )
{
    int useless;
    bool phase_active = search4power( useless, POW_PHASEBALL, powers );
    bool fire_active = search4power( useless, POW_FIREBALL, powers );

    if( state == 1 )
    {
        x = padx + PAD_W/2 - BALL_W/2;
        y = PAD_Y - BALL_W/2;
    }
    else if( state == 2 )
    {
        //making the ball leave a trail
        spawn_particle( x + BALL_W/2, y + BALL_W/2, TRAIL, clip.x, effects );

        int dx, dy;
        float vectorLength;

        dx =  (int)(dirX * vel * dt);
        dy =  (int)(dirY * vel * dt);

        x += dx;
        y += dy;

        //bouncing from the walls
        if( ( x < 100 ) || ( x > 682 ) )
        {
            //playing sound
            Mix_PlayChannel( -1, SFX_bump_wall, 0 );
            //keeping them borders
            if( x < 100 ) x = 101;
            if( x > 682 ) x = 683;

            dirX = -dirX;
        }
        //bouncing from the ceiling
        if ( y < 50 )
        {
            //playing sound
            Mix_PlayChannel( -1, SFX_bump_wall, 0 );

            y = 51;
            dirY = -dirY;
        }
         //bouncing from the paddle
        if( ( x + BALL_W/2 >= padx ) && ( x + BALL_W/2 <= padx + PAD_W ) && ( y <= PAD_Y + PAD_H - BALL_W/2 ) && ( y >= PAD_Y - BALL_W/2 ))
        {
            //playing sound
            Mix_PlayChannel( -1, SFX_bump_pad, 0 );

            //adjusting ball position if it barely hits the paddle
            if( x + BALL_W <= padx && ( y > PAD_Y && y < PAD_Y + PAD_H ) )
            {
                y = PAD_Y + BALL_W + 5;
                x = padx - BALL_W - 3;
            }
            if( x >= padx + PAD_W && ( y > PAD_Y && y < PAD_Y + PAD_H ) )
            {
                y = PAD_Y + BALL_W + 5;
                x = padx + PAD_W + 3;
            }
            else
            {
                y = PAD_Y - BALL_W/2;
                x -= dx;
            }

            //actual collision response
            dirY = -dirY;
            dirX = ( ( ( x + (float)BALL_W/2 ) - ( padx + (float)PAD_W/2 ) ) / ( (float)PAD_W / 8 ) );

            //normalization of the direction vector:
            vectorLength = sqrt( (dirX*dirX) + (dirY*dirY) );
            dirX = dirX / vectorLength;

            //increasing speed
            if( vel < BALL_MAXV )
                vel += 5;
        }

        //bouncing from the bricks
        for( int i = 0; i < vect.size(); i++ )
        {
            if( ( x + BALL_W >= vect[i].getX()) && ( x <= vect[i].getX() + BRICK_W ) && ( y + BALL_W >= vect[i].getY() ) && ( y <= vect[i].getY() + BRICK_H ) )
            {
                //playing sound
                Mix_PlayChannel( -1, SFX_hit, 0 );
                if(vect[i].getType() == EXPLOSIVE ) Mix_PlayChannel( -1, SFX_explode, 0 );

                if( fire_active ) vect[i].setType( EXPLOSIVE );
                if( !phase_active )
                {
                    x -= dx;
                    y -= dy;

                    //determine which side of the brick the ball is hitting:
                    //a) left/right
                    if(( x + BALL_W >= vect[i].getX() || x <= vect[i].getX() + BRICK_W ) && ( y <= (vect[i].getY() + BRICK_H ) && ( y + BALL_W >= vect[i].getY() ) ) )
                    {
                        dirX = -dirX;
                    }
                    //b) bottom/top
                    else
                    {
                        dirY = -dirY;
                    }
                }
                hitBrick_r( i, ( fire_active || phase_active ), 0, vect, killvect );
            }
        }

    }
}
void Ball::change_state( int st )
{
    //st: 0 = out, 1 = held, 2 = moving

    if( st == 0 )
    {
        state = 0;
    }

    if( st == 1 )
    {
        state = 1;
        vel = 0;
        dirX = 0.0;
        dirY = 1.0;
    }

    if ( st == 2 && state != 2 && state != 0 )
    {
        state = 2;
        vel = 280;
    }
}
void Ball::deathcheck( int &lives, gamestate_t &gState, int &padVX )
{
    if( y > PAD_Y + 25 && state == 2)
    {
        change_state( 0 );
        lives--;

        if( lives >= 0)
        {
            Mix_PlayChannel( -1, SFX_balldie, 0 );
            change_state( 1 );
        }
        else
        {
            gState = STATE_LOST;
            Mix_PlayChannel( -1, SFX_death, 0 );
        }
    }
}