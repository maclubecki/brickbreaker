#include "button.h"

Button::Button( bool startbutton )
{
    use_index = 0;
    start_button = startbutton;
    clips[0].x = 0;
    clips[0].y = 0;
    clips[0].w = 140;
    clips[0].h = 40;

    clips[1].x = 140;
    clips[1].y = 0;
    clips[1].w = 140;
    clips[1].h = 40;

    if( startbutton == true )
    {
        pos_y = 250;
    }
    else
    {
        pos_y = 320;
    }

    pos_x = WID/2 - clips[0].w/2;
}
bool Button::is_selected( int _x, int _y )
{
    if( _x >= pos_x && _x <= pos_x + clips[0].w && _y >= pos_y && _y <= pos_y + clips[0].h )
    {
        use_index = 1;
        return true;
    }
    else
    {
        use_index = 0;
        return false;
    }
}
void Button::show()
{
    apply_surface( pos_x, pos_y, menu_buttons, screen, &clips[ use_index ] );

    //applying button text
    if( start_button )
    {
        SDL_FreeSurface( menu_buttons_start );
        menu_buttons_start = TTF_RenderText_Solid( font_score, "START GAME", textColor );
        apply_surface( pos_x+15, pos_y+10, menu_buttons_start, screen );
    }
    else
    {
        SDL_FreeSurface( menu_buttons_end );
        menu_buttons_end = TTF_RenderText_Solid( font_score, "QUIT", textColor );
        apply_surface( pos_x + 50, pos_y+10, menu_buttons_end, screen );
    }
}