#include "global_brick_fs.h"

void spawn_particle( int x, int y, particle_t type, int _magnitude, std::vector <Particle> &particles )
{
    Particle *a;
    particles.push_back( *( a = new Particle( x, y, type, _magnitude ) ) );
    delete a;
}
//make explosion
void spawn_explosion( int x, int y, std::vector <Particle> &particles )
{
    Mix_PlayChannel(-1, SFX_exploding, 0);
    Particle *a;

    for( int i = 1; i < 9; i++ )
    {
        particles.push_back( *( a = new Particle( x, y, SPARKS, i ) ) );
        delete a;
    }
}
//pickup explosions
void spawn_pickup( int x, int y, std::vector <Particle> &particles )
{
    Particle *a;

    for( int i = 2; i <= 8; i++ )
    {
        particles.push_back( *( a = new Particle( x, y, PICKUP, i ) ) );
        delete a;
    }
}
//brick search
bool search4brick( int row, int column, int &index, std::vector <Brick> &vect )
{
    /*
    This function looks for a brick with the mentioned row and column.
    If it finds the brick, it returns true and stores it's index in the index variable.
    Otherwise it returns false.
    */

    if( row < 0 || row > 13 || column < 0 || column > 11)
        return false;

    for( int i = 0; i < vect.size(); i++ )
    {
        if( vect.at(i).getRow() == row && vect.at(i).getColumn() == column )
        {
            index = i;
            return true;
        }
    }

    return false;
}
//find a powerup in the vector
bool search4power( int &index, powertype_t type, std::vector <Powerup> &powers_act )
{
    for( int i = 0; i < powers_act.size() ; i++ )
    {
        if( type == powers_act.at(i).getType() )
        {
            index = i;
            return true;
        }
    }

    return false;
}
//brick hit
void hitBrick_r( int index, bool blown, int timemod, std::vector <Brick> &vect, std::vector <Brick> &killvect )
{
    /*
    The brick of 'index' gets it's hp substracted by the Brick::hit() function.
    If it's hp is less or equal to 0, then it gets erased from the brick vector.
    Furthermore, if it's an explosive brick, the surrounding bricks are getting 'hit'
        recursively.
    If the 'blown' var is true, indestructible bricks get destroyed.

    The 's' in the 's_...' vars stands for SEARCH, as they're used for the search4brick().
    */

    int decoy;
    const int DELAY = 100;//explosion delay

    vect.at(index).hit( blown );

    if( vect.at(index).getType() == EXPLOSIVE )
    {
        int s_row, s_col, s_i;

        s_row = vect.at(index).getRow();
        s_col = vect.at(index).getColumn();

        if( vect[index].getHP() <= 0 && search4brick( vect[index].getRow(), vect[index].getColumn(), decoy, killvect ) == false )
        {
            //set the values
            vect.at(index).setTOD( SDL_GetTicks() + timemod );
            vect.at(index).setIsbroken();
            //get it on the vector of bricks-to-be-erased
            killvect.push_back( vect.at(index) );
        }

        //hitting all surrounding bricks
        if( search4brick( s_row+1 , s_col , s_i , vect ) == true && search4brick( s_row+1 , s_col , decoy , killvect ) == false )
        {
            hitBrick_r( s_i, true, timemod + DELAY, vect , killvect);
        }
        if( search4brick( s_row-1 , s_col , s_i , vect ) == true && search4brick( s_row-1 , s_col , decoy , killvect ) == false)
        {
            hitBrick_r( s_i, true, timemod + DELAY, vect , killvect);
        }
        if( search4brick( s_row , s_col - 1 , s_i, vect ) == true && search4brick( s_row , s_col - 1 , decoy , killvect ) == false)
        {
            hitBrick_r( s_i, true, timemod + DELAY, vect , killvect);
        }
        if( search4brick( s_row , s_col + 1 , s_i, vect ) == true && search4brick( s_row , s_col + 1 , decoy , killvect ) == false)
        {
            hitBrick_r( s_i, true, timemod + DELAY, vect , killvect);
        }
    }
    else
    {
        if( vect.at(index).getHP() <= 0 && search4brick( vect.at(index).getRow(), vect.at(index).getColumn(), decoy, killvect ) == false)
        {
            //set the values
            vect.at(index).setTOD( SDL_GetTicks() + timemod );
            vect.at(index).setIsbroken();
            //get it on the vector of bricks-to-be-erased
            killvect.push_back( vect.at(index) );
        }
    }
}
//erase all 'dead' bricks
void eraseBricks( long &score, Camera &cam, std::vector <Brick> &vect , std::vector <Brick> &killvect , std::vector <Particle> &particles, std::vector <Powerup> &powers_inact, std::vector <Powerup> &powers_act )
{
    /*
    This function is supposed to find ALL the 'dead' bricks and erase them when
    a specific time has passed.
    That time is stored in every brick object as 'time_odeath'.
    The while loop checks the killvect structure of pointers to dead bricks.

    The 'found' bool checks if a brick which is supposed to be dead has been found.
    It's supposed to be dead if the current time exceeds it's time of death.

    If it has been found, the brick and a pointer to it are erased from their
    respective vectors.
    */

    bool found = false, double_active = false;
    int k_index, v_index, pts, multiplier;
    int z;//garbage

    double_active = search4power( z, POW_DOUBLEPTS, powers_act );
    if( double_active ) multiplier = 2;
    else multiplier = 1;

    while( killvect.size() > 0 )
    {

        for( int i = 0; i < killvect.size(); i ++ )
        {
            if( SDL_GetTicks() >= killvect.at(i).getTOD() )
            {
                k_index = i;
                found = true;
                break;
            }
        }

        //if no brick suitable to be erased
        if( found == false )
        {
            break;
        }
        //erase the pointer and the brick if "it's time is up"
        if( found == true && search4brick( killvect.at(k_index).getRow(), killvect.at(k_index).getColumn(), v_index, vect ) == true )
        {
            score += killvect.at(k_index).getPoints() * multiplier;
            //spawning score particle
            spawn_particle( killvect.at(k_index).getX() + BRICK_W/2, killvect.at(k_index).getY() + BRICK_H/2, SCORE, killvect.at(k_index).getPoints() * multiplier, particles );
            //spawning explosion particle
            spawn_particle( killvect.at(k_index).getX() + BRICK_W/2 - 6, killvect.at(k_index).getY() + BRICK_H/2 - 5, EXPLOSION, 1, particles );
            //spawning sparks if its an explosive brick
            if( killvect.at(k_index).getType() == EXPLOSIVE )
            {
               spawn_explosion( killvect.at(k_index).getX() + BRICK_W/2 - 6, killvect.at(k_index).getY() + BRICK_H/2 - 5, particles );
               //shaking the camera
               cam.shaking = true;
               cam.shake_time = SDL_GetTicks();
            }
            //dropping powerups
            if( killvect.at(k_index).getDoesDrop() )
            {
                Mix_PlayChannel(-1, SFX_drop, 0);
                //setting random direction
                int sign = random_gen(1, 10);
                if( sign <= 5 ) sign = -1; else sign = 1;

                Powerup *p;
                powers_inact.push_back( *( p = new Powerup( killvect.at(k_index).getX(), killvect.at(k_index).getY(), sign, killvect.at(k_index).getDropType() ) ) );
                delete p;
            }

            killvect.erase(  killvect.begin() + k_index );
            vect.erase(  vect.begin() + v_index );

            found = false;
        }
    }
}