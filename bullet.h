#ifndef BULLET_H_INCLUDED
#define BULLET_H_INCLUDED

class Bullet
{
    private:

        int x, y;
        bool phasing;
        bool dead;

    public:

        Bullet( int _padx, bool _phaseOn );

        int getX();
        int getY();

        bool isDead();

        void move( float dt, std::vector <Particle> &particles );
        void update( float dt, std::vector <Particle> &particles, std::vector <Brick> &vect, std::vector <Brick> &killvect );
};


#endif // BULLET_H_INCLUDED
