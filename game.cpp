#include "game.h"

Game::Game()
{
    mouse_x = 0; mouse_y = 0;
    level = 1;
    lives = 2;
    score = 0;
    pause_time = 0;
    state = STATE_MENU;

    startBtn = new Button( true );
    quitBtn = new Button( false );

    cam.x = 0;
    cam.y = 0;
    cam.shaking = false;
    cam.shake_time = 0;

    msgOrigin.x = WID/2 - 175;
    msgOrigin.y = -180;
    msgVel = 900;
}
gamestate_t Game::getState()
{
    return state;
}
void Game::setState( gamestate_t s )
{
    state = s;
}
bool Game::winCheck()
{
    for( int i = 0 ; i < bricks.size() ; i++ )
    {
        if( bricks[i].getType() != INDESTRUCTIBLE )
        {
            return false;
        }
    }
    state = STATE_WON;
    return true;
}
void Game::displayEverything()
{
    //camera shakes or not
    if( cam.shaking == true && cam.shake_time + SHAKE_DURATION >= SDL_GetTicks() )
    {
        //cam.x++;
        cam.y--; //BEWARE HARD SHAKES
    }
    if( cam.shake_time + SHAKE_DURATION <= SDL_GetTicks() )
    {
        cam.shaking = false;
        cam.x = 0;
        cam.y = 0;
    }

    //displaying stuff
    apply_surface( 0 + cam.x, 0 + cam.y, img, screen );
    for( int i = 0; i < bricks.size(); i++ )
    {
        bricks.at(i).show( cam.x, cam.y );
    }
    for( int i = 0; i < effects.size(); i++ )
    {
        effects.at(i).show( cam.x, cam.y );
    }
    for( int i = 0; i < powers_inactive.size(); i++ )
    {
        powers_inactive.at(i).show();
    }
    playerPaddle.show( cam.x, cam.y );
    playBall.show( cam.x, cam.y );
    showUI();
}
void Game::handleMouseInput( SDL_Event &e , bool &quit )
{
    int x, y;

    x = e.motion.x;
    y = e.motion.y;

    mouse_x = x; mouse_y = y;

    if( e.type == SDL_MOUSEBUTTONDOWN )
    {
        if( e.button.button == SDL_BUTTON_LEFT )
        {
            if( startBtn->is_selected( mouse_x, mouse_y ) )
            {
                setState( STATE_PLAYING );
                init_bricks();
                //hiding the cursor
                SDL_ShowCursor(0);
                return;
            }
            if( quitBtn->is_selected( mouse_x, mouse_y ) )
            {
                quit = true;
                return;
            }
        }
    }
}
void Game::showUI()
{
    SDL_Rect clip;
    clip.x = 0;
    clip.y = 0;
    clip.w = 12;
    clip.h = 12;

    std::stringstream txt1, txt2;

    txt1 << "SCORE: " << score;
    SDL_FreeSurface( surf_score );
    surf_score = TTF_RenderText_Solid( font_score, txt1.str().c_str(), textColor );
    apply_surface( 100, 560, surf_score, screen );

    txt2 << "LEVEL: " << level;
    SDL_FreeSurface( surf_level );
    surf_level = TTF_RenderText_Solid( font_score, txt2.str().c_str(), textColor );
    apply_surface( 100, 545, surf_level, screen );

    SDL_FreeSurface( surf_lives );
    surf_lives = TTF_RenderText_Solid( font_score, "LIVES: ", textColor );
    apply_surface( 100, 530, surf_lives, screen );

    for(int i = 0; i < lives ; i++ )
    {
        apply_surface( 150 + (i*BALL_W + 15), 532, ball_surf, screen, &clip );
    }

    SDL_FreeSurface( surf_powers_txt );
    surf_powers_txt = TTF_RenderText_Solid( font_score, "ACTIVE POWERS: ", textColor );
    apply_surface( 100, 575, surf_powers_txt, screen );

    for(int i = 0; i < powers_active.size() ; i++ )
    {
        apply_surface( 255 + (16 * i) , 577, pow_sheet, screen, powers_active.at(i).getClip() );
    }

}
//Important functions:
int Game::init_bricks_setGaps( int r_cols )
{
    //possible number of gaps - number of columns:
    // 0 - n%2 == 1 - nieparzyste
    // 1 - n%2 == 0 - parzyste
    // 2 - 3 or 9
    // 3 - 8

    int rand = random_gen( 1, 100, 20 );

    if( r_cols % 2 == 0 )
    {
        if( r_cols == 8 )
        {
            if( rand >= 50 ) return 3;
        }
        return 1;
    }
    if( r_cols % 2 == 1 )
    {
        if( r_cols == 3 || r_cols == 9 )
        {
            if( rand >= 50 ) return 2;
        }

        return 0;
    }
}
int Game::init_bricks_setShift( int r_cols, int r_gaps )
{
    //The function returns the number of columns to be shifted:
    //the 0 gap shift starts at the beggining
    //the 1 gap shift starts after half of the row has been placed
    //the 2 gap shift happens after 1/3 of the number of cols has been placed
    //the 3 gap shift happens every 2 bricks placed

    if( r_gaps == 0 ) return ( (MAX_COLUMNS - r_cols)/2 );
    if( r_gaps == 1 ) return (   MAX_COLUMNS - r_cols   );
    if( r_gaps == 2 )
    {
        if( r_cols == 3 ) return 8;
        else return 2;
    }
    if( r_gaps == 3 ) return 3;
}
type_t Game::init_bricks_setTypes( int r_cols, int i )
{
    int r = random_gen( 1, 4, i );

    if( r == 1 ) return REGULAR;
    if( r == 2 && r_cols < 9 ) return REINFORCED;
    if( r == 3 ) return EXPLOSIVE;
    if( r == 4 && r_cols < 8 ) return INDESTRUCTIBLE;

    return REGULAR;

    /*
    if( r <= 45 ) return REGULAR;
    if( r > 45 && r <= 75 ) return REINFORCED;
    if( r > 75 && r <= 90 ) return EXPLOSIVE;
    if( r > 90 && r <= 100) return INDESTRUCTIBLE;
        */
}
void Game::init_bricks()
{
    //Description: Initializes a new level.
    //int scv = a supporting variable for modifying when the shift is supposed to take place
    int r_cols, r_rows, r_gaps;
    int shift_cols, scv, shift_x;
    int rand;

    //randomizing the number of rows total and the number of columns in the first row
    r_rows = random_gen( 3, MAX_ROWS, 10 );
    r_cols = random_gen( 3, MAX_COLUMNS, 15 );
    r_gaps = init_bricks_setGaps( r_cols );

    //a shift of columns that 'centers' the bricks
    shift_cols = init_bricks_setShift( r_cols, r_gaps );
    shift_x = 0;
    scv = 0;

    type_t currentType = EXPLOSIVE;

    //GENERATING LEVEL:
    for( int i = 0; i < r_rows; i++ )
    {
        for( int j = 0; j < r_cols; j++ )
        {
            if( r_gaps == 0 )
            {
                scv = shift_cols;
                shift_x = scv * BRICK_W;
            }
            if( r_gaps == 1 && j == r_cols/2 )
            {
                scv = shift_cols;
                shift_x = scv * BRICK_W;
            }
            if( r_gaps == 2 && (j == r_cols/3 || j == (2 * r_cols)/3 ) )
            {
                if( r_cols == 3 )
                {
                    if( scv < shift_cols ) scv+=4;
                    shift_x = BRICK_W * scv;
                }
                else
                {
                    if( scv < shift_cols ) scv++;
                    shift_x += BRICK_W;
                }
            }
            if( r_gaps == 3 && ( j == r_cols/4 || j == 2*r_cols/4 || j == 3*r_cols/4 ) )
            {
                if( scv < shift_cols ) scv++;
                shift_x += BRICK_W;
            }

            Brick *b;
            bricks.push_back( *( b = new Brick(( shift_x + 125 + ( j * BRICK_W ) ), ( 85 + ( i * BRICK_H ) ), i, j + scv, currentType )));
            delete b;
        }

        //randomizing number of columns and gaps
        r_cols = random_gen( 3, MAX_COLUMNS, i);
        r_gaps = init_bricks_setGaps( r_cols );
        //adjusting column shift and resetting the gap shift
        shift_cols = init_bricks_setShift( r_cols, r_gaps );
        scv = 0;
        shift_x = 0;
        currentType = init_bricks_setTypes( r_cols, i );
    }
}
void Game::init_powers()
{
    for( int i = 0; i < bricks.size(); i++ )
    {
        bricks.at(i).setDrop( i );
    }
}
void Game::clearParticles()
{
    bool found = false;
    int dead_index = 0;

    while( true )
    {
        for( int i = 0; i < effects.size(); i++ )
        {
            if( effects[i].isDead() )
            {
                dead_index = i;
                found = true;
                break;
            }
        }

        if( !found )
        {
            break;
        }
        else
        {
            effects.erase( effects.begin() + dead_index );
            found = false;
        }
    }
}
void Game::clearBullets()
{
    bool found = false;
    int dead_index = 0;

    while( true )
    {
        for( int i = 0; i < bullets.size(); i++ )
        {
            if( bullets[i].isDead() )
            {
                dead_index = i;
                found = true;
                break;
            }
        }

        if( !found )
        {
            break;
        }
        else
        {
            Mix_PlayChannel(-1, SFX_laserhit, 0 );
            bullets.erase( bullets.begin() + dead_index );
            found = false;
        }
    }
}
void Game::clearPowerups( std::vector <Powerup> &powers )
{
    bool found = false;
    int dead_index = 0;

    while( true )
    {
        for( int i = 0; i < powers.size(); i++ )
        {
            if( powers[i].shouldDie() )
            {
                dead_index = i;
                found = true;
                break;
            }
        }

        if( !found )
        {
            break;
        }
        else
        {
            powers.erase( powers.begin() + dead_index );
            found = false;
        }

    }
}
void Game::msgScreen( float dt, bool wonGame )
{
    //Display stuff:
    displayEverything();

    if( msgOrigin.y <= HIG )
    {
        //move it
        msgOrigin.y += (int)( msgVel * dt);
        //manipulating the speed so it looks smooth n shit
        if( msgOrigin.y < HIG / 2 - 150 ) msgVel -= 10;
        if( msgOrigin.y >= HIG / 2 - 150 && msgOrigin.y <= HIG / 2 - 100 ) msgVel = 200;
        if( msgOrigin.y > HIG / 2 - 100 ) msgVel += 100;

        //show it
        SDL_Rect clip;
        if( wonGame )
        {
            clip.x = 0;
            clip.y = 0;
            clip.w = 330;
            clip.h = 160;
        }
        else
        {
            clip.x = 0;
            clip.y = 160;
            clip.w = 330;
            clip.h = 160;
        }
        apply_surface( msgOrigin.x, msgOrigin.y, gratz_surf, screen, &clip );
    }

    //Logic stuff, happens after message is gone
    if( msgOrigin.y > HIG )
    {
        bricks.clear();
        bricks_broken.clear();
        bullets.clear();
        effects.clear();
        powers_active.clear();
        powers_inactive.clear();

        if( wonGame )
        {
            init_bricks();
            init_powers();
        }
        if( !wonGame )
        {
            lives = 2;
            score = 0;
        }
        playerPaddle.center();
        playBall.change_state(1);

        //resetting the message settings
        msgOrigin.y = -200;
        msgVel = 900;//must be the same as the one in the constructor

        if( wonGame )
        {
            level++;
            state = STATE_PLAYING;
        }
        else
        {
            level = 1;
            SDL_ShowCursor(1);
            state = STATE_MENU;
        }
    }
}
void Game::startup( bool &quit )
{
    //this is gonna be the first thing that you see when you
    //launch the game
    //also get accumulator for those functions' arguments?
    SDL_Event mouseEvent;

    //getting input
    if( SDL_PollEvent( &mouseEvent ) )
    {
        handleMouseInput( mouseEvent, quit );

        if( mouseEvent.type == SDL_QUIT )
        {
            quit = true;
            return;
        }
    }

    //checking which sprite for the buttons should be used
    startBtn->is_selected( mouse_x, mouse_y );
    quitBtn->is_selected( mouse_x, mouse_y );

    //Display everything in the menu:
    //filling screen with color as a background
    apply_surface( 0, 0, menu_logo, screen );
    startBtn->show();
    quitBtn->show();

    //initalizing powerups
    init_powers();
}
void Game::play( bool &quit, float dt, float &accumulator )
{

    //Handling input:
    if( SDL_PollEvent( &event ) )
    {
        playerPaddle.handle_input( playBall, state, pause_time, powers_active, bullets );

        if( event.type == SDL_QUIT )
        {
            quit = true;
        }
    }

    //Logic updates:
    while( accumulator >= dt )
    {
        playerPaddle.move( dt, effects, powers_active, powers_inactive, lives, playBall.getV() );
        playBall.move( dt, playerPaddle.getX(), playerPaddle.getMotion(), bricks, bricks_broken, powers_active, effects );
        eraseBricks( score, cam, bricks, bricks_broken, effects, powers_inactive, powers_active );
        clearParticles();
        clearBullets();
        clearPowerups( powers_active );
        clearPowerups( powers_inactive );
        playBall.deathcheck( lives, state, playerPaddle.xVel );
        for( int i = 0; i < powers_inactive.size(); i++ )
        {
            powers_inactive.at(i).update( dt );
        }
        for( int i = 0; i < effects.size(); i++ )
        {
            effects.at(i).update( dt, effects );
        }
        for( int i = 0; i < bullets.size(); i++ )
        {
            bullets.at(i).update( dt, effects, bricks, bricks_broken );
        }
        playBall.update_clip( powers_active );
        if( winCheck() ) Mix_PlayChannel( -1, SFX_lvlwon, 0 );
        accumulator -= dt;
    }


    //Display:
    displayEverything();

}
void Game::paused( bool &quit, float &frame_time )
{
    std::stringstream txt;
    float df;

    SDL_FreeSurface(pause_msg);
    pause_msg = TTF_RenderText_Solid( font_score, "GAME PAUSED", textColor );
    SDL_SetAlpha( black_sc, SDL_SRCALPHA, 125 );
    df = (float)((SDL_GetTicks() - pause_time)/1000.f);

    //getting input
    if( SDL_PollEvent( &event ) )
    {
        //key released
        if( event.type == SDL_KEYUP )
        {
            if( event.key.keysym.sym == SDLK_ESCAPE )
            {
                state = STATE_PLAYING;
                Mix_PlayChannel( -1, SFX_pickup, 0 );
                SDL_ShowCursor(0);
                frame_time -= df;
                //delaying the death of bricks based on time the game was paused
                for( int i = 0; i < bricks_broken.size(); i++ )
                {
                    bricks_broken.at(i).setTOD( bricks_broken[i].getTOD() + (int)(df*1000) );
                }
                for( int i = 0; i < powers_active.size(); i++ )
                {
                    powers_active.at(i).extendTime( (int)(df*1000) );
                }
                return;
            }
        }

        if( event.type == SDL_QUIT )
        {
            quit = true;
        }
    }

     //displaying stuff
    displayEverything();
    apply_surface( 0, 0, black_sc, screen );
    apply_surface( 342, 285, pause_msg, screen );

}
