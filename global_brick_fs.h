#ifndef GLOBAL_BRICK_FS_H_INCLUDED
#define GLOBAL_BRICK_FS_H_INCLUDED

//GLOBAL BRICK RELATED FUNCTIONS


//spawning a particle
void spawn_particle( int x, int y, particle_t type, int _magnitude, std::vector <Particle> &particles );
//make explosion
void spawn_explosion( int x, int y, std::vector <Particle> &particles );
//pickup explosions
void spawn_pickup( int x, int y, std::vector <Particle> &particles );
//brick search
bool search4brick( int row, int column, int &index, std::vector <Brick> &vect );
//find a powerup in the vector
bool search4power( int &index, powertype_t type, std::vector <Powerup> &powers_act );
//brick hit
void hitBrick_r( int index, bool blown, int timemod, std::vector <Brick> &vect, std::vector <Brick> &killvect );
//erase all 'dead' bricks
void eraseBricks( long &score, Camera &cam, std::vector <Brick> &vect , std::vector <Brick> &killvect , std::vector <Particle> &particles, std::vector <Powerup> &powers_inact, std::vector <Powerup> &powers_act );

#endif // GLOBAL_BRICK_FS_H_INCLUDED
