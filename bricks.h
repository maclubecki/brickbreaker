#include "globals.h"

#ifndef BRICKS_H_INCLUDED
#define BRICKS_H_INCLUDED

class Brick
{
    private:

        int x, y;
        int row, column;
        int hp;
        int bonus_chance;
        int points;
        long time_odeath;

        type_t brickType;
        powertype_t dropType;
        SDL_Rect clip;

        bool isbroken;
        bool doesDrop;

    public:

        Brick( int posx, int posy, int p_row, int p_column, type_t typ );

        int getX();
        int getY();
        int getRow();
        int getColumn();
        int getHP();
        int getPoints();
        long getTOD();
        bool getIsbroken();
        bool getDoesDrop();

        type_t getType();
        powertype_t getDropType();

        void show( int cx, int cy );
        void hit( bool blown );
        void setTOD( long x );//time of death
        void setIsbroken();
        void setDrop( int i );
        void setType( type_t newType );
};


#endif // BRICKS_H_INCLUDED
