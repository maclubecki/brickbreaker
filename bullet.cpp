#include "bullet.h"

Bullet::Bullet( int _padx, bool _phaseOn )
{
    x = _padx + PAD_W/2;
    y = PAD_Y;
    phasing = _phaseOn;
    dead = false;
}

bool Bullet::isDead()
{
    return dead;
}

void Bullet::move( float dt, std::vector <Particle> &particles )
{
    int dy;
    int mag = 0;
    if( phasing ) mag = 1;

    dy = (int)(-1 * 1000 * dt);//dir, vel, time

    //make a trail here
    Particle *a;

    particles.push_back( *( a = new Particle( x, y, TRAIL_BULLET, mag ) ) );
    delete a;

    y += dy;
}
void Bullet::update( float dt, std::vector <Particle> &particles, std::vector <Brick> &vect, std::vector <Brick> &killvect )
{
    //first check if bullet is out of bounds
    if( y <= 50 )
    {
        dead = true;
        return;
    }
    //next check brick collision
    //if yes, then remove brick and make dead = true
        for( int i = 0; i < vect.size(); i++ )
        {
            if( ( x >= vect[i].getX()) && ( x + 3 <= vect[i].getX() + BRICK_W ) && ( y <= vect[i].getY() ) )
            {
                if( !phasing )
                {
                    dead = true;
                }

                hitBrick_r( i, true, 0, vect, killvect );
                break;
            }
        }

    move( dt, particles );
}