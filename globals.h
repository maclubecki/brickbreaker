#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"
#include <string>
#include <cmath>
#include <vector>
#include <fstream>
#include <sstream>

#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

//GLOBAL VARS

const int WID = 800;
const int HIG = 600;
const int BPP = 32;

const int PAD_W = 100;
const int PAD_H = 20;
const int PAD_Y = 460;
const int PAD_V = 550;

const int BALL_W = 12;
const int BALL_MAXV = 380;
const float BALL_MAX_DIR = 1.2f;

const int BRICK_W = 50;
const int BRICK_H = 20;
const int MAX_COLUMNS = 11;
const int MAX_ROWS = 13;

const int MSG_V = 20;

const int SHAKE_DURATION = 40;

//general surfaces
SDL_Surface* screen = NULL;
SDL_Surface* img = NULL;
SDL_Surface* loge = NULL;
SDL_Surface* surf_level = NULL;
SDL_Surface* surf_score = NULL;
SDL_Surface* surf_lives = NULL;
SDL_Surface* surf_powers_txt = NULL;
SDL_Surface* error_msg = NULL;

//game related surfaces
SDL_Surface* menu_buttons = NULL;
SDL_Surface* menu_buttons_start = NULL;
SDL_Surface* menu_buttons_end = NULL;
SDL_Surface* menu_logo = NULL;
SDL_Surface* pad_surf = NULL;
SDL_Surface* ball_surf = NULL;
SDL_Surface* gratz_surf = NULL;

//powerup sheet
SDL_Surface* pow_package = NULL;
SDL_Surface* pow_sheet = NULL;

//brick sheet
SDL_Surface* brick_sheet = NULL;

//particle sheet
SDL_Surface* particles = NULL;

//pause screen related surfaces
SDL_Surface* pause_msg = NULL;
SDL_Surface* black_sc = SDL_CreateRGBSurface( 0, 800, 600, 32, 0, 0, 0, 0);

//sound effects
Mix_Chunk* SFX_bump_pad = NULL;
Mix_Chunk* SFX_bump_wall = NULL;
Mix_Chunk* SFX_hit = NULL;
Mix_Chunk* SFX_firelaser = NULL;
Mix_Chunk* SFX_pickup = NULL;
Mix_Chunk* SFX_death = NULL;
Mix_Chunk* SFX_explode = NULL;
Mix_Chunk* SFX_exploding = NULL;
Mix_Chunk* SFX_drop = NULL;
Mix_Chunk* SFX_balldie = NULL;
Mix_Chunk* SFX_lvlwon = NULL;
Mix_Chunk* SFX_laserhit = NULL;

//font and the white color for it
TTF_Font* font_score = NULL;
SDL_Color textColor = { 255, 255, 255 };

//global event
SDL_Event event;

struct Point
{
    int x, y;
};
enum type_t //brick types
{
    REGULAR,
    REINFORCED,
    EXPLOSIVE,
    INDESTRUCTIBLE
};

enum particle_t
{
    SCORE,
    EXPLOSION,
    TRAIL,
    TRAIL_PAD,
    TRAIL_SPARK,
    TRAIL_BULLET,
    SPARKS,
    PICKUP
};

enum powertype_t
{
    POW_LIFE = 0,
    POW_LAZOR,
    POW_PHAZOR,
    POW_SLOWBALL,
    POW_FASTBALL,
    POW_FIREBALL,
    POW_PHASEBALL,
    POW_TRIBALL,
    POW_DOUBLEPTS
};

enum gamestate_t
{
    STATE_MENU,
    STATE_PLAYING,
    STATE_PAUSED,
    STATE_WON,
    STATE_LOST
};

struct Camera
{
    int x, y;
    long shake_time;
    bool shaking;
};

//GLOBAL FUNCTIONS

//float minimum
float min( float a, float b );
//point intersection
bool intersects_pad( int xObj, int yObj, int xPad );
//RNG
int random_gen( int a, int b, int x = NULL );
//loading imgs
SDL_Surface* load_img( std::string filename );
//applying imgs
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* dest , SDL_Rect* clip = NULL );
//initializing all
bool init();
//mass load files
bool load_files();
//cleanup
void cleanup();

#endif // GLOBALS_H_INCLUDED
