#ifndef BUTTON_H_INCLUDED
#define BUTTON_H_INCLUDED

class Button
{
    private:

        int pos_x, pos_y, use_index;
        bool start_button;
        SDL_Rect clips[2];//[1] is highlighted, [0] isnt

    public:

        Button( bool startbutton );
        bool is_selected( int _x, int _y );
        void show();

};


#endif // BUTTON_H_INCLUDED
